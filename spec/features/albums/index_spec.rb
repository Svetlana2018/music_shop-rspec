require 'rails_helper'

RSpec.feature 'Album list' do
  scenario 'unauthenticated user' do
    visit albums_path # method capibara
    within '#content' do
      expect(find('h1')).to have_content 'Albums'
    end
  end
end