require 'rails_helper'

RSpec.describe 'albums/index' do
  subject { build(:album) }
  it 'render _album partial for each album' do
    # https://github.com/rspec/rspec-mocks/blob/62af5b2ee82e5628643d04ced7a7441e3a7a9f94/lib/rspec/mocks/example_methods.rb
    # https://github.com/rspec/rspec-mocks/issues/1102
    without_partial_double_verification do
      assign(:albums, [subject, subject])
      expect(view).to receive(:current_user).and_return(nil)
      render
      expect(view).to render_template(partial: '_album', count: 2)
    end

    # render template: 'project/index'
    # render partial: '...'
    # render 'albums/album', album: album
  end

  it "displays album's title" do
    without_partial_double_verification do
    assign(:albums, [subject])
    render
    expect(rendered).to include(subject.title)
    end
    end

end