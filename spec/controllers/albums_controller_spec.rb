require 'rails_helper'

RSpec.describe AlbumsController  do
  let(:album) {build(:album)}
  describe 'POST #create' do

      let(:params) { params = ActionController::Parameters.new({title: 'Test'}).permit!}
      it 'create new album' do
        expect(Album).to receive(:new).with(params).and_return(album)
        expect(album).to receive(:save).with(no_args).and_return true
        post :create, params: {album: {title: 'Test'}}
        expect(response).to redirect_to(albums_path)
        expect(flash[:success]).to eq 'Album was created!'
      end

   end

  describe 'GET #new' do
    it 'instantiates a new album' do
      expect(Album).to receive(:new).with(no_args)
      get :new
    end

  end

  describe 'GET #index' do
    # before(:each) {get :index}
    # успешно отвечает с кодом статуса HTTP 200
    it 'responds successfully with an HTTP 200 status code' do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)

    end

    # успешно отображает шаблон index
    it 'renders the index template' do
      get :index
      expect(response).to render_template('index')
    end


      # загружает все альбомы в переменную экземпляра @albums
      it 'loads all albums into the @albums' do
        expect(Album).to receive(:order).with('created_at DESC').and_return([album])
        get :index
        expect(assigns(:albums)).to match_array([album])
      end

  end
end

