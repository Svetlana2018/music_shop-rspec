FactoryBot.define do
  factory :cart_item do
    user
    # association :user, factory: :user, name: 'Test User', strategy: :build_stubbed
    album
    quantity 1
  end
end
