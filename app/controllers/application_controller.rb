class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  #include AuthenticationHelper

protected

  def current_user
      @current_user ||= User.first
  end

  def require_login
    redirect_to root_path, notice: 'You are not logged in!' unless @current_user
  end

  helper_method :current_user
end
